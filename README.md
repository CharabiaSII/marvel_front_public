# Marvel_Front


## Configuration

- Projet Angular (14.2)
- NodeJS (16.17)
- Visual Studio Code (1.71.1)

## Dépendances

- Angular Material
- Ngx-infinite-scroll
- Ngx-toastr

## Observations

Prévoir authentification pour changer d'utilisateur.

Par défaut, utilisateur ID 1.