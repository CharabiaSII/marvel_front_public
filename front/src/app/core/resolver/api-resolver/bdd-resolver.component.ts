import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Hero } from '../../model/Hero';
import { ApiBddService } from '../../service/api-bdd.service';
import { ApiMarvelService } from '../../service/api-marvel.service';

@Injectable()
export class BddResolver implements Resolve<Hero[]> {
  constructor(private apiBddService: ApiBddService, private apiMarvelService : ApiMarvelService) {}

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Hero[]> {
    console.log('resolver')
    const listId = await this.apiBddService.getFavourites(1);
    return this.apiMarvelService.getListHeros(listId);
  }
}