import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Data, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Hero } from '../../model/Hero';
import { ApiMarvelService } from '../../service/api-marvel.service';

@Injectable()
export class ApiResolver implements Resolve<Hero[]> {
  constructor(private apiService: ApiMarvelService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Hero[]> {
    console.log('resolver')
    return this.apiService.getHeros();
  }
}