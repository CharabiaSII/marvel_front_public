import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Data } from '@angular/router';
import { Hero } from '../../model/Hero';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input() heroes!: Hero[];
  @Input() page!: string;
  @Output() idHero : EventEmitter<Object> = new EventEmitter();


  constructor() { }

  ngOnInit(): void {
  }

  emitId(idHero: number, nameHero : string){
    this.idHero.emit({idHero: idHero, nameHero: nameHero});
  }

}
