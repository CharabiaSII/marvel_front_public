import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { Favorites } from '../model/Favorites';
import { Hero } from '../model/Hero';
import { ApiBddService } from '../service/api-bdd.service';
import { ApiMarvelService } from '../service/api-marvel.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit {

  heroes : Hero[] = [];
  scrollCounter: number = 0;
  page = "all";

  constructor(
    private route: ActivatedRoute, 
    private apiService: ApiMarvelService, 
    private apiBddSErvice : ApiBddService,
    private toastr: ToastrService) { }

  async ngOnInit() {
    let routeData = await firstValueFrom( this.route.data );

    this.heroes = routeData['heroes'];
  }

  async onScroll() {
    let heroes = await this.apiService.getHeros(++this.scrollCounter);

    console.log(this.scrollCounter, heroes, [...heroes]);

    this.heroes = this.heroes.concat( heroes );
  }

  async addFavHero(object: any){
    try{
      let favorites = await this.apiBddSErvice.saveFavourite(new Favorites(1, object.idHero));
      if(favorites.idhero == object.idHero){
        this.toastr.success('Le héro ' + object.nameHero + ' a été ajouté à la liste de favoris');
      }
      else{
        this.toastr.error("Erreur dans l'enregistrement");
      }
    }catch(error){
      this.toastr.error("Ce héro a déjà été ajouté");
    }

  }

}
