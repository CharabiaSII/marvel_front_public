export class Hero {
    id! : number;
    name! : string;
    description! : string;
    path! : string; 
    extension! : string; 
}
