import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { Favorites } from '../model/Favorites';
import { Hero } from '../model/Hero';
import { ApiBddService } from '../service/api-bdd.service';
import { ToastrService } from 'ngx-toastr';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.scss']
})
export class FavouritesComponent implements OnInit {

  heroes : Hero[] = [];
  page = "fav";

  constructor(
    private route: ActivatedRoute, 
    private apiBddService : ApiBddService,
    private toastr: ToastrService) { }

  async ngOnInit() {
    let routeData = await firstValueFrom( this.route.data );

    this.heroes = routeData['heroes'];
  }

  async removeFavHero(object: any){
    try{
      let favorites = await this.apiBddService.deleteFavourite(new Favorites(1, object.idHero));
      if(favorites.status == 204){
        const index = this.heroes.findIndex(hero => {
          return hero.id === object.idHero;
        });
        this.heroes.splice(index,1);
        this.toastr.success('Le héro ' + object.nameHero + ' a été supprimé la liste de favoris');
      }
      else{
        this.toastr.error("Erreur dans la suppression");
      }
    }catch(error){
      this.toastr.error("Erreur dans la suppression");
    }

  }

}
