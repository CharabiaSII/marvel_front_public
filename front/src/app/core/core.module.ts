import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { ApiMarvelService } from './service/api-marvel.service';
import { HttpClientModule } from '@angular/common/http';
import { ApiResolver } from './resolver/api-resolver/api-resolver.component';
import { CoreComponent } from './core.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { BddResolver } from './resolver/api-resolver/bdd-resolver.component';
import { ApiBddService } from './service/api-bdd.service';
import { HeroesComponent } from './heroes/heroes.component';
import { FavouritesComponent } from './favourites/favourites.component';


@NgModule({
  declarations: [
    CoreComponent,
    HeroesComponent,
    FavouritesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    HttpClientModule,
    InfiniteScrollModule,
  ],
  exports: [
  ],
  providers: [
    ApiMarvelService,
    ApiBddService,
    ApiResolver,
    BddResolver
  ]
})
export class CoreModule { }
