import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { firstValueFrom, map, Observable, tap } from 'rxjs';
import { Hero } from './model/Hero';
import { ApiMarvelService } from './service/api-marvel.service';

@Component({
  selector: 'app-core',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.scss']
})
export class CoreComponent {

  
}
