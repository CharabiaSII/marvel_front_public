import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Favorites } from '../model/Favorites';

@Injectable({
  providedIn: 'root'
})
export class ApiBddService {

  constructor(private http: HttpClient) {}

  getFavourites(user: Number): Promise<number[]> {    
    return firstValueFrom( this.http.get<number[]>(`${environment.apiBdd}/user/`+ user) );
  }

  saveFavourite(favorites: Favorites): Promise<Favorites> {    
    return firstValueFrom(this.http.post<Favorites>(`${environment.apiBdd}/addfav`, favorites ));
  }

  deleteFavourite(favorites: Favorites): Promise<HttpResponse<any>> {    
    return firstValueFrom(this.http.delete(`${environment.apiBdd}/removefav/`, {body : favorites, observe: 'response' }));
  }
}
