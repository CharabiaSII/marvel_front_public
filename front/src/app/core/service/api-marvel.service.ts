import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Data } from '@angular/router';
import { firstValueFrom, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Hero } from '../model/Hero';


@Injectable({
  providedIn: 'root'
})
export class ApiMarvelService {

  constructor(private http: HttpClient) {}

  getHeros(page? : number): Promise<Hero[]> {
    let param = new HttpParams().set('page', page? page : 0);
    
    return firstValueFrom( this.http.get<Hero[]>(`${environment.apiMarvel}/all`, { params: param }) );
  }

  getListHeros(list : number[]): Promise<Hero[]> {
    let param = new HttpParams().set('idHeroes', list.toString());
    
    return firstValueFrom( this.http.get<Hero[]>(`${environment.apiMarvel}/byHero`, { params: param }) );
  }
}
