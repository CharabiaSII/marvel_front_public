import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FavouritesComponent } from './core/favourites/favourites.component';
import { HeroesComponent } from './core/heroes/heroes.component';
import { ApiResolver } from './core/resolver/api-resolver/api-resolver.component';
import { BddResolver } from './core/resolver/api-resolver/bdd-resolver.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', redirectTo : 'list', pathMatch : 'full' },
  //{ path: 'menu', loadChildren: () => import('./core/core.module').then(m => m.CoreModule) }
  { path: 'list', component: HeroesComponent, resolve: { heroes: ApiResolver }},
  { path: 'fav', component: FavouritesComponent, resolve: { heroes: BddResolver }}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
